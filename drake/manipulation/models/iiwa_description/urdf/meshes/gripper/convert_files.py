#This is for converting a bunch of mesh files all at once.

import os

os.system('sudo apt-get install openctm-tools')

files = [f for f in os.listdir('.') if os.path.isfile(f)]

'''
input_extension = ".STL"
output_extension = ".dae"
for f in files:
    filename, file_extension = os.path.splitext(f)

    if file_extension == input_extension:
        print filename , file_extension
        i = filename + input_extension
        o = filename + output_extension
        command = "ctmconv " + i + " " + o
        os.system(command)

input_extension = ".dae"
output_extension = ".stl"
for f in files:
    filename, file_extension = os.path.splitext(f)

    if file_extension == input_extension:
        print filename , file_extension
        i = filename + input_extension
        o = filename + output_extension
        command = "ctmconv " + i + " " + o
        os.system(command)

input_extension = ".stl"
output_extension = ".dae"
for f in files:
    filename, file_extension = os.path.splitext(f)

    if file_extension == input_extension:
        print filename , file_extension
        i = filename + input_extension
        o = filename + output_extension
        command = "ctmconv " + i + " " + o
        os.system(command)
'''
input_extension = ".stl"
output_extension = ".obj"
for f in files:
    filename, file_extension = os.path.splitext(f)

    if file_extension == input_extension:
        print filename , file_extension
        i = filename + input_extension
        o = filename + output_extension
        command = "ctmconv " + i + " " + o + " --calc-normals"
        os.system(command)
'''
input_extension = ".obj"
output_extension = ".dae"
for f in files:
    filename, file_extension = os.path.splitext(f)

    if file_extension == input_extension:
        print filename , file_extension
        i = filename + input_extension
        o = filename + output_extension
        command = "ctmconv " + i + " " + o
        os.system(command)
'''
