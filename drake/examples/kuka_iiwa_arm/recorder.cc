#include <iostream>
#include <fstream>
#include <lcm/lcm-cpp.hpp>
#include "drake/lcmt_iiwa_command.hpp"
#include "drake/lcmt_iiwa_status.hpp"
#include "drake/lcmt_robotiq_ft_sensor.hpp"
#include "drake/lcmt_object_pose.hpp"


class Recorder {
	lcm::LCM my_lcm;
	drake::lcmt_iiwa_status pub_msg;

public:
	int utime;
 	std::vector<double> joint_position_measured;
  std::vector<double> joint_position_commanded;
  std::vector<double> joint_velocity_estimated;
	std::vector<double> joint_torque_commanded;
  std::vector<double> engine_pose;
  std::vector<double> ft_wrench;

	std::ofstream datafile;

  double Kp_array[7] = {100,100,100,100,100,100,100};
  double Kd_array[7] = {20,20,20,20,20,20,20};
  std::vector<double> Kp;
  std::vector<double> Kd;
  double dt;
  int num_joints;

  // Initialize subscribers, fill vectors
	void initialize()
	{
		num_joints = 7;
		my_lcm.subscribe("IIWA_STATUS",&Recorder::status_cb,this);
		my_lcm.subscribe("IIWA_COMMAND",&Recorder::command_cb,this);
    my_lcm.subscribe("FT_SENSOR",&Recorder::ft_sensor_cb,this);
    my_lcm.subscribe("ENGINE_POSE",&Recorder::engine_pose_cb,this);
		joint_position_measured.resize(num_joints,0);
		joint_velocity_estimated.resize(num_joints,0);
		joint_torque_commanded.resize(num_joints,0);
    ft_wrench.resize(6,0);
    engine_pose.resize(6,0);
		datafile.open("kuka_data.txt");
	}

  	// Update member variables for positions and velocities
    void status_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan,const drake::lcmt_iiwa_status* status)
    {
    	// Estimate the joint velocities
    	dt = (status->utime - utime)/1e6;
    	//dt = (status->utime - utime);
    	for(int joint=0;joint<num_joints;joint++)
    	{
    		joint_velocity_estimated[joint] = (status->joint_position_measured[joint]-joint_position_measured[joint])/dt;
    	}

    	// Update Positions
    	utime = status->utime;
    	joint_position_measured = status->joint_position_measured;

    	// Write the current state and command to file
    	datafile<<utime/1e6<<",";
    	for(int i=0;i<7;i++)
    	{
    		datafile<<joint_position_measured[i]<<",";
    	}

    	for(int i=0;i<7;i++)
    	{
    		datafile<<joint_velocity_estimated[i]<<",";
    	}

    	for(int i=0;i<7;i++)
      {
        datafile<<joint_torque_commanded[i]<<",";
      }

      for(int i=0;i<6;i++)
      {
        datafile<<engine_pose[i]<<",";
      }

      for(int i=0;i<6;i++)
      {
        datafile<<ft_wrench[i]<<",";
      }


		  datafile<<"\n";


    	//datafile<<joint_position_measured<<"\t"<<joint_velocity_estimated<<"\t"<<joint_torque_commanded<<"\n";

		std::cout<<"\n"<<utime/1e6<<"\n";

    /*
		std::cout<<"\n"<<joint_position_measured[0];
		std::cout<<"  "<<joint_position_measured[1];
		std::cout<<"  "<<joint_position_measured[2];
		std::cout<<"  "<<joint_position_measured[3];
		std::cout<<"  "<<joint_position_measured[4];
		std::cout<<"  "<<joint_position_measured[5];
		std::cout<<"  "<<joint_position_measured[6];

		std::cout<<"\n"<<joint_velocity_estimated[0];
		std::cout<<"  "<<joint_velocity_estimated[1];
	 	std::cout<<"  "<<joint_velocity_estimated[2];
		std::cout<<"  "<<joint_velocity_estimated[3];
		std::cout<<"  "<<joint_velocity_estimated[4];
		std::cout<<"  "<<joint_velocity_estimated[5];
		std::cout<<"  "<<joint_velocity_estimated[6]<<"\n";
		*/
    }

    // Callback for IIWA_COMMAND messages
    void command_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan,const drake::lcmt_iiwa_command* command)
    {
      // Retrieve the desired joint torques from the IIWA_COMMAND message
    joint_torque_commanded = command->joint_torque;

    /*
		std::cout<<"\n"<<joint_torque[0];
		std::cout<<"  "<<joint_torque[1];
		std::cout<<"  "<<joint_torque[2];
		std::cout<<"  "<<joint_torque[3];
		std::cout<<"  "<<joint_torque[4];
		std::cout<<"  "<<joint_torque[5];
		std::cout<<"  "<<joint_torque[6];
    */
    }

    // Callback for FT_SENSOR messages
    void ft_sensor_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan,const drake::lcmt_robotiq_ft_sensor* msg)
    {
      // Retrieve the desired joint torques from the IIWA_COMMAND message
      for(int i=0;i<6;i++)
      {
        ft_wrench[i] = msg->wrench[i];  
      }
      
    }

    // Callback for ENGINE_POSE messages
    void engine_pose_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan,const drake::lcmt_object_pose* msg)
    {
      // Retrieve the desired joint torques from the IIWA_COMMAND message
      for(int i=0;i<6;i++)
      {
        engine_pose[i] = msg->pose[i];  
      }
    }


  	
    void spin()
    {
    	while(1)
    	{
    		my_lcm.handle();
    	}
    }

};

int main()
{

	Recorder mysub;
	mysub.initialize();

	mysub.spin();

	mysub.datafile.close();
	return 0;
}
