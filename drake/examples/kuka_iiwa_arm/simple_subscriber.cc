#include <iostream>
#include <lcm/lcm-cpp.hpp>
#include "drake/lcmt_iiwa_command.hpp"
#include "drake/lcmt_iiwa_status.hpp"


class SimpleSubscriber {
	lcm::LCM my_lcm;
	drake::lcmt_iiwa_status pub_msg;

public:
	int utime;
 	std::vector<double> joint_position_measured;
  	std::vector<double> joint_position_commanded;
   	std::vector<double> joint_velocity_estimated;
	std::vector<double> joint_torque;
	std::vector<double> joint_torque_measured;
	std::vector<double> joint_torque_commanded;
	std::vector<double> joint_torque_external;

   	double Kp_array[7] = {100,100,100,100,100,100,100};
   	double Kd_array[7] = {20,20,20,20,20,20,20};
   	std::vector<double> Kp;
   	std::vector<double> Kd;
   	double dt;
   	int num_joints;

   	// Initialize subscribers, fill vectors
	void initialize()
	{
		num_joints = 7;
		my_lcm.subscribe("IIWA_STATUS",&SimpleSubscriber::status_cb,this);
		my_lcm.subscribe("IIWA_TORQUE_COMMAND",&SimpleSubscriber::command_cb,this);
		joint_position_measured.resize(num_joints,0);
		joint_position_commanded.resize(num_joints,0);
		joint_velocity_estimated.resize(num_joints,0);
		joint_torque.resize(num_joints);
		Kp.resize(num_joints);
		Kd.resize(num_joints);
		for(int i=0;i<num_joints;i++)
		{
			Kp[i] = Kp_array[i];
			Kd[i] = Kd_array[i];
		}
	}

	// Takes a desired torque and publishes the position required to acheive that torque to IIWA_COMMAND
	void publish_positions(std::vector<double> desired_torques)
	{
	  drake::lcmt_iiwa_command iiwa_command;
	  iiwa_command.num_joints = num_joints;
	  iiwa_command.joint_position.resize(num_joints, 0.);
 	  iiwa_command.num_torques = num_joints;
  	  iiwa_command.joint_torque.resize(num_joints, 0.);

	  // Do math to figure out what positions will apply the desired torques
	  for(int i=0;i<num_joints;i++)
	    {
	      iiwa_command.joint_position[i] = (desired_torques[i]+Kp[i]*joint_position_measured[i]+Kd[i]*joint_velocity_estimated[i])/Kp[i];
	    }
	  my_lcm.publish("IIWA_COMMAND",&iiwa_command);
	}
  
  	// Update member variables for positions and velocities
    void status_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan,const drake::lcmt_iiwa_status* status)
    {
    	// Estimate the joint velocities
    	dt = (status->utime - utime)/1e6;
    	//dt = (status->utime - utime);
    	for(int joint=0;joint<num_joints;joint++)
    	{
    		joint_velocity_estimated[joint] = (status->joint_position_measured[joint]-joint_position_measured[joint])/dt;
    	}

    	// Update Positions
    	utime = status->utime;
    	joint_position_measured = status->joint_position_measured;
    	joint_position_commanded = status->joint_position_commanded;
    	joint_torque_measured = status->joint_torque_measured;
    	joint_torque_external = status->joint_torque_external;
    	joint_torque_commanded = status->joint_torque_commanded;

		//std::cout<<"\n"<<dt<<"\n";

    	
		std::cout<<"\n"<<joint_torque_external[0];
		std::cout<<"  "<<joint_torque_external[1];
		std::cout<<"  "<<joint_torque_measured[2];
		std::cout<<"  "<<joint_torque_measured[3];
		std::cout<<"  "<<joint_torque_commanded[4];
		std::cout<<"  "<<joint_torque_commanded[5];
		std::cout<<"  "<<joint_torque_measured[6];

		std::cout<<"\n"<<joint_position_measured[0];
		std::cout<<"  "<<joint_position_measured[1];
		std::cout<<"  "<<joint_position_measured[2];
		std::cout<<"  "<<joint_position_measured[3];
		std::cout<<"  "<<joint_position_measured[4];
		std::cout<<"  "<<joint_position_measured[5];
		std::cout<<"  "<<joint_position_measured[6];

		std::cout<<"\n"<<joint_velocity_estimated[0];
		std::cout<<"  "<<joint_velocity_estimated[1];
	 	std::cout<<"  "<<joint_velocity_estimated[2];
		std::cout<<"  "<<joint_velocity_estimated[3];
		std::cout<<"  "<<joint_velocity_estimated[4];
		std::cout<<"  "<<joint_velocity_estimated[5];
		std::cout<<"  "<<joint_velocity_estimated[6]<<"\n";
		

		
    }

    // Callback for IIWA_TORQUE_COMMAND messages
    void command_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan,const drake::lcmt_iiwa_command* command)
    {
    	// Retrieve the desired joint torques from the IIWA_TORQUE_COMMAND message
		joint_torque = command->joint_torque;

/*
		std::cout<<"\n"<<joint_torque[0];
		std::cout<<"  "<<joint_torque[1];
		std::cout<<"  "<<joint_torque[2];
		std::cout<<"  "<<joint_torque[3];
		std::cout<<"  "<<joint_torque[4];
		std::cout<<"  "<<joint_torque[5];
		std::cout<<"  "<<joint_torque[6];
*/
    }


  	
    void spin()
    {
    	while(1)
    	{
    		my_lcm.handle();
	
			// Calculate position needed to acheive torque and publish that position
			publish_positions(joint_torque);
    	}
    }

};

int main()
{

	SimpleSubscriber mysub;
	mysub.initialize();

	mysub.spin();

	return 0;
}
