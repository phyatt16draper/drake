#include <iostream>
#include <lcm/lcm-cpp.hpp>
#include "drake/lcmt_iiwa_command.hpp"
#include "drake/lcmt_iiwa_status.hpp"



int main()
{
	lcm::LCM my_lcm;
	drake::lcmt_iiwa_status pub_msg;

	int num_joints = 7;
	std::vector<double> my_desired_positions;
	my_desired_positions.resize(7);

	while(1)
	{
		std::cout<<"\nEnter the joint angles you want\n";
		for(int i=0;i<7;i++)
		{
			double num;
			std::cin>>num;
			my_desired_positions[i] = num;
		}
		
		// Publish the qdes to the impedance controller
    	drake::lcmt_iiwa_command iiwa_command;
  		iiwa_command.num_joints = num_joints;
 	    iiwa_command.num_torques = num_joints;
  	    iiwa_command.joint_torque.resize(num_joints, 0.);
  	    iiwa_command.joint_position.resize(7,0);
  	    for(int i=0;i<7;i++)
	    {
	      iiwa_command.joint_position[i] = my_desired_positions[i];
   	      iiwa_command.joint_torque[i] = my_desired_positions[i];
	    }
	    my_lcm.publish("IIWA_QDES_COMMAND",&iiwa_command);
		std::cout<<"\nPublished qdes\n";
	}

	return 0;
}
