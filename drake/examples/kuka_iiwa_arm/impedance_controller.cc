/*
This is just a simple Impedance Controller to drive the KUKA iiwa
arm to joint positions published on IIWA_QDES_COMMAND
*/

#include <iostream>
#include <lcm/lcm-cpp.hpp>
#include "drake/lcmt_iiwa_command.hpp"
#include "drake/lcmt_iiwa_status.hpp"
#include "drake/multibody/parsers/urdf_parser.h"
#include "drake/multibody/rigid_body_tree_construction.h"
#include "drake/common/drake_path.h"

#include "drake/systems/framework/diagram.h"
#include "drake/systems/framework/diagram_builder.h"

#include "drake/common/drake_assert.h"
#include "drake/common/drake_path.h"
#include "drake/examples/kuka_iiwa_arm/iiwa_common.h"
#include "drake/examples/kuka_iiwa_arm/iiwa_lcm.h"
#include "drake/examples/kuka_iiwa_arm/sim_diagram_builder.h"
#include "drake/lcm/drake_lcm.h"

#include "drake/lcmt_iiwa_command.hpp"
#include "drake/lcmt_iiwa_status.hpp"
#include "drake/lcmt_robotiq_ft_sensor.hpp"
#include "drake/lcmt_object_pose.hpp"


#include "drake/multibody/parsers/urdf_parser.h"
#include "drake/multibody/rigid_body_plant/drake_visualizer.h"
#include "drake/multibody/rigid_body_plant/rigid_body_plant.h"
#include "drake/multibody/rigid_body_tree_construction.h"
#include "drake/systems/analysis/simulator.h"
#include "drake/systems/controllers/inverse_dynamics_controller.h"
#include "drake/systems/framework/diagram.h"
#include "drake/systems/framework/diagram_builder.h"
#include "drake/systems/framework/leaf_system.h"
#include "drake/systems/lcm/lcm_publisher_system.h"
#include "drake/systems/lcm/lcm_subscriber_system.h"
#include "drake/systems/primitives/constant_vector_source.h"

#include "drake/lcmt_contact_results_for_viz.hpp"
#include "drake/lcmt_contact_info_for_viz.hpp"

#include <math.h>

//using systems::RigidBodyPlant;



class ImpedanceController {
	lcm::LCM my_lcm;

public:
	// The timestamp in microseconds.
	int utime;
	int num_contacts;
	std::vector<double> resultant_contact_force;
 	std::vector<double> joint_position_measured;
  	std::vector<double> joint_torque_commanded;
   	std::vector<double> joint_velocity_estimated;
   	std::vector<double> joint_acceleration_estimated;
   	std::vector<double> last_joint_velocity_estimated;
    	
	std::vector<double> qdes;
   	double dt;
	static const int num_joints = 7;
	double Kp[num_joints] = {2,2,2,2,1,1,1};
   	double Kd[num_joints] = {1.9,2.9,1.9,1.9,.1,.1,.1};

   	// Make a rigid body tree that we can use to do estimation stuff
	const std::string kModelPath = "/manipulation/models/iiwa_description/urdf/"
  	"iiwa14_polytope_collision.urdf";
	const std::string my_urdf = (drake::GetDrakePath() + kModelPath);

	RigidBodyTree<double> tree;

   	


    

	
	ImpedanceController()
	{
		my_lcm.subscribe("IIWA_STATUS",&ImpedanceController::status_cb,this);
		my_lcm.subscribe("IIWA_QDES_COMMAND",&ImpedanceController::command_cb,this);
		my_lcm.subscribe("CONTACT_RESULTS",&ImpedanceController::contact_cb,this);
		joint_position_measured.resize(num_joints,0);
		joint_torque_commanded.resize(num_joints,0);
		joint_velocity_estimated.resize(num_joints,0);
		joint_acceleration_estimated.resize(num_joints,0);
		last_joint_velocity_estimated.resize(7);
		qdes.resize(7);
		resultant_contact_force.resize(6);

		for(int i=0;i<num_joints;i++)
		{
			qdes[i] = 0;
		}

		drake::parsers::urdf::AddModelInstanceFromUrdfFileToWorld(my_urdf, drake::multibody::joints::kFixed, &tree);
        
        
	}

	void contact_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan,const drake::lcmt_contact_results_for_viz* msg)
	{
		num_contacts = msg->num_contacts;

		//std::cout<<"This many contacts: "<<num_contacts<<"\n";
		drake::lcmt_contact_info_for_viz contacts;

		for(int i=0;i<6;i++)
		{
			resultant_contact_force[i] = 0;
		}

		// Get the transform from world to the ft_sensor
		double *qptr = &joint_position_measured[0];
	    Eigen::Map<Eigen::VectorXd> q(qptr, 7);
	    double *qdptr = &joint_velocity_estimated[0];
	    Eigen::Map<Eigen::VectorXd> qd(qdptr, 7);
    	KinematicsCache<double> cache = tree.doKinematics(q, qd);

		int base_body_or_frame_ind = 0;
		int force_torque_body_or_frame_ind = 12;
		auto ft_sensor_pose = tree.relativeTransform(cache,base_body_or_frame_ind,force_torque_body_or_frame_ind);

		Eigen::Matrix3d R0_ft;
		R0_ft << ft_sensor_pose(0,0), ft_sensor_pose(0,1), ft_sensor_pose(0,2),
			     ft_sensor_pose(1,0), ft_sensor_pose(1,1), ft_sensor_pose(1,2),
			   	 ft_sensor_pose(2,0), ft_sensor_pose(2,1), ft_sensor_pose(2,2);

		for(int i=0;i<num_contacts;i++)
		{
			contacts = msg->contact_info[i];
			/*
			std::cout<<contacts.contact_point[0]<<"\t"<<contacts.contact_point[1]<<"\t"<<contacts.contact_point[2]<<"\n";
			std::cout<<contacts.contact_force[0]<<"\t"<<contacts.contact_force[1]<<"\t"<<contacts.contact_force[2]<<"\n";
			std::cout<<contacts.normal[0]<<"\t"<<contacts.normal[1]<<"\t"<<contacts.normal[2]<<"\n";
			std::cout<<"\n";
			*/

			// Get the vector from the FT sensor to the point of contact
			Eigen::Vector3d r0p;
			r0p(0) = contacts.contact_point[0] - ft_sensor_pose(0,3);
			r0p(1) = contacts.contact_point[1] - ft_sensor_pose(1,3);
			r0p(2) = contacts.contact_point[2] - ft_sensor_pose(2,3);

			// Torque at sensor = (vector_from_FT_sensor_to_point_of_contact) x (Force_at_point_of_contact)
			Eigen::Vector3d tau_ft;
			Eigen::Vector3d F_p;
			F_p(0) = contacts.contact_force[0];
			F_p(1) = contacts.contact_force[1];
			F_p(2) = contacts.contact_force[2];

			tau_ft = r0p.cross(F_p);


			// Force at sensor = (Force_at_point_of_contact)

			// Rotate Force and Torque to be in ft_sensor frame
			F_p = R0_ft.transpose()*F_p;
			tau_ft = R0_ft.transpose()*tau_ft;


			resultant_contact_force[0] = resultant_contact_force[0] + F_p(0);
			resultant_contact_force[1] = resultant_contact_force[1] + F_p(1);
			resultant_contact_force[2] = resultant_contact_force[2] + F_p(2);
			resultant_contact_force[3] = resultant_contact_force[3] + tau_ft(0);
			resultant_contact_force[4] = resultant_contact_force[4] + tau_ft(1);
			resultant_contact_force[5] = resultant_contact_force[5] + tau_ft(2);
		}

		drake::lcmt_robotiq_ft_sensor ft_sensor;
		
	  	for(int i=0;i<6;i++)
	    {
	      ft_sensor.wrench[i] = resultant_contact_force[i];
	    }

	  	my_lcm.publish("FT_SENSOR",&ft_sensor);

		std::cout<<"resultant_contact_force\n";
		for(int i=0;i<6;i++)
			{
				std::cout<<resultant_contact_force[i]<<"  ";
			}
			std::cout<<"\n";
		
	}

	void task_space_transform()
	{
		double *qptr = &joint_position_measured[0];
	    Eigen::Map<Eigen::VectorXd> q(qptr, 7);
	    double *qdptr = &joint_velocity_estimated[0];
	    Eigen::Map<Eigen::VectorXd> qd(qdptr, 7);
	    KinematicsCache<double> cache = tree.doKinematics(q, qd);

		// Calculate the Jacobian
		int base_body_or_frame_ind = 0;
		int end_effector_body_or_frame_ind = 20;
		int expressed_in_body_or_frame_ind = 0;
		bool in_terms_of_qdot = 0;

		std::vector<int> v_or_qdot_indices;
		drake::TwistMatrix<double> J = tree.geometricJacobian(cache, base_body_or_frame_ind, end_effector_body_or_frame_ind, expressed_in_body_or_frame_ind, in_terms_of_qdot, &v_or_qdot_indices);
		Eigen::MatrixXd	JT_inv = (J*J.transpose()).inverse()*J;
		Eigen::MatrixXd	J_inv = (J.transpose()*J).inverse()*J.transpose();

		Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> A = tree.massMatrix(cache);
		Eigen::MatrixXd Lambda = JT_inv*A*J_inv;


        Eigen::VectorXd b_and_g(7);
        Eigen::VectorXd zeros(7); // 7DOF jonit position
        zeros.setZero();
        const RigidBodyTree<double>::BodyToWrenchMap no_external_wrenches;
        b_and_g = tree.inverseDynamics(cache,no_external_wrenches,zeros,false);

        auto Jdot_times_qdot = tree.geometricJacobianDotTimesV(cache,base_body_or_frame_ind,end_effector_body_or_frame_ind,expressed_in_body_or_frame_ind);

        Eigen::VectorXd mu_and_p = JT_inv*b_and_g - Lambda*Jdot_times_qdot;

        Eigen::VectorXd tau(7);
        for(int i=0;i<7;i++)
        {
        	tau(i) = joint_torque_commanded[i];	
        }
        
        Eigen::VectorXd Fee = J.transpose()*tau;

        Eigen::VectorXd xddot = Lambda.inverse()*(mu_and_p + Fee);



        Eigen::VectorXd accel = Eigen::VectorXd::Zero(7); 

        for(int i=0;i<7;i++)
        {
        	accel(i) = joint_acceleration_estimated[i];
        }

        Eigen::VectorXd xddot_meas = J*accel+Jdot_times_qdot;

        /*
        std::cout<<"Xddot estimate based on dynamics\n";
        std::cout<<xddot<<"\n";
        std::cout<<"Xddot measured (kinda)\n";        
        std::cout<<xddot_meas<<"\n";
        std::cout<<"Difference\n";        
        std::cout<<xddot_meas-xddot<<"\n";
		*/
	}

	void get_engine_pose()
	{
		double *qptr = &joint_position_measured[0];
	    Eigen::Map<Eigen::VectorXd> q(qptr, 7);
	    double *qdptr = &joint_velocity_estimated[0];
	    Eigen::Map<Eigen::VectorXd> qd(qdptr, 7);
    	KinematicsCache<double> cache = tree.doKinematics(q, qd);

		int base_body_or_frame_ind = 0;
		int end_effector_body_or_frame_ind = 12;
		//drake::Transform<double, 3, Isometry> ee_pose = tree.relativeTransform(cache,base_body_or_frame_ind,end_effector_body_or_frame_ind);
		auto ee_pose = tree.relativeTransform(cache,base_body_or_frame_ind,end_effector_body_or_frame_ind);

		//std::cout<<"FK calculated from frame: "<<tree.getBodyOrFrameName(base_body_or_frame_ind)<<"\n";
        //std::cout<<"FK calculated to frame: "<<tree.getBodyOrFrameName(end_effector_body_or_frame_ind)<<"\n";


		// THIS IS JUST HARDCODED HERE - IN REAL LIFE IT SHOULD COME FROM SEGICP
		Eigen::MatrixXd R_world_engine(3,3);
		R_world_engine << 0, 0, 1,
						  1, 0, 0,
						  0, 1, 0;

		Eigen::VectorXd P_world_engine(3);
		P_world_engine << -1,
						  0,
						  .75;



		Eigen::MatrixXd R_world_ee(3,3);
		R_world_ee << ee_pose(0,0), ee_pose(0,1), ee_pose(0,2),
					  ee_pose(1,0), ee_pose(1,1), ee_pose(1,2),
					  ee_pose(2,0), ee_pose(2,1), ee_pose(2,2);

		Eigen::MatrixXd R_ee_engine = R_world_ee.transpose()*R_world_engine;

		double yaw = atan2(R_ee_engine(1,0),R_ee_engine(0,0));
		double pitch = atan2(-R_ee_engine(2,0),sqrt(pow(R_ee_engine(2,1),2)+pow(R_ee_engine(2,2),2)));
		double roll = atan2(R_ee_engine(2,1),R_ee_engine(2,2));

		Eigen::VectorXd P_engine(6);
		P_engine << P_world_engine(0) - ee_pose(0,3),
					P_world_engine(1) - ee_pose(1,3),
					P_world_engine(2) - ee_pose(2,3),
					yaw,
					pitch,
					roll;

		//std::cout<<"\n\nEngine Pose wrt EE\n"<<P_engine<<"\n";

		drake::lcmt_object_pose engine_pose;
		//engine_pose.relative_engine_pose.resize(6,0);
	  	for(int i=0;i<6;i++)
	    {
	      engine_pose.pose[i] = P_engine(i);
	    }
	  	my_lcm.publish("ENGINE_POSE",&engine_pose);

	}


	void get_fk()
	{
		double *qptr = &joint_position_measured[0];
	    Eigen::Map<Eigen::VectorXd> q(qptr, 7);
	    double *qdptr = &joint_velocity_estimated[0];
	    Eigen::Map<Eigen::VectorXd> qd(qdptr, 7);
    	KinematicsCache<double> cache = tree.doKinematics(q, qd);

		int base_body_or_frame_ind = 0;
		int end_effector_body_or_frame_ind = 20;
		//drake::Transform<double, 3, Isometry> ee_pose = tree.relativeTransform(cache,base_body_or_frame_ind,end_effector_body_or_frame_ind);
		auto ee_pose = tree.relativeTransform(cache,base_body_or_frame_ind,end_effector_body_or_frame_ind);

		
		std::cout<<"EE Pose:\n";
		std::cout<<ee_pose(0,0)<<"\t"<<ee_pose(0,1)<<"\t"<<ee_pose(0,2)<<"\t"<<ee_pose(0,3)<<"\n";
		std::cout<<ee_pose(1,0)<<"\t"<<ee_pose(1,1)<<"\t"<<ee_pose(1,2)<<"\t"<<ee_pose(1,3)<<"\n";
		std::cout<<ee_pose(2,0)<<"\t"<<ee_pose(2,1)<<"\t"<<ee_pose(2,2)<<"\t"<<ee_pose(2,3)<<"\n";
		std::cout<<ee_pose(3,0)<<"\t"<<ee_pose(3,1)<<"\t"<<ee_pose(3,2)<<"\t"<<ee_pose(3,3)<<"\n";
		
	}

	void calculate_ee_force()
	{
		double *qptr = &joint_position_measured[0];
	    Eigen::Map<Eigen::VectorXd> q(qptr, 7);
	    double *qdptr = &joint_velocity_estimated[0];
	    Eigen::Map<Eigen::VectorXd> qd(qdptr, 7);

		std::vector<double> zeros_vector;   	    
		zeros_vector.resize(7);

   		double *my_zeroptr = &zeros_vector[0];
	    Eigen::Map<Eigen::VectorXd> zero_qd(my_zeroptr, 7);

	    KinematicsCache<double> gravity_cache = tree.doKinematics(q,zero_qd);


		// Calculate the Jacobian for this position
		int base_body_or_frame_ind = 0;
		int end_effector_body_or_frame_ind = 12;
		int expressed_in_body_or_frame_ind = 0;
		bool in_terms_of_qdot = 0;
		std::vector<int> v_or_qdot_indices;
		//auto J = tree.relativeRollPitchYawJacobian(cache, base_body_or_frame_ind, end_effector_body_or_frame_ind, in_terms_of_qdot);
		drake::TwistMatrix<double> J = tree.geometricJacobian(gravity_cache, base_body_or_frame_ind, end_effector_body_or_frame_ind, expressed_in_body_or_frame_ind, in_terms_of_qdot, &v_or_qdot_indices);

        //std::cout<<"Jacobian calculated from frame: "<<tree.getBodyOrFrameName(base_body_or_frame_ind)<<"\n";
        //std::cout<<"Jacobian calculated to frame: "<<tree.getBodyOrFrameName(end_effector_body_or_frame_ind)<<"\n";
        //std::cout<<"Jacobian expressed in frame: "<<tree.getBodyOrFrameName(expressed_in_body_or_frame_ind)<<"\n";


        // Calculate the gravity compensation torques
	    Eigen::VectorXd zeros(7);
        zeros.setZero();
        Eigen::VectorXd tau_gravity(7);
        const RigidBodyTree<double>::BodyToWrenchMap no_external_wrenches;

        tau_gravity = tree.inverseDynamics(gravity_cache,no_external_wrenches,zeros,false);

   		//std::cout<<"Gravity compensation torques:\n";	
		//std::cout<<tau_gravity<<"\n";

        // Calculate the torques causing acceleration + gravity compensation torques
        KinematicsCache<double> cache = tree.doKinematics(q, qd);
        Eigen::VectorXd tau_accel(7);
        Eigen::VectorXd accel = Eigen::VectorXd::Zero(7); 

        for(int i=0;i<7;i++)
        {
        	accel(i) = joint_acceleration_estimated[i];
        }
        
        tau_accel = tree.inverseDynamics(cache,no_external_wrenches,accel,false);

        // Calculate the torques caused by external wrenches
		Eigen::VectorXd tau(7);
		Eigen::VectorXd F(6);
		
		for(int i=0;i<7;i++)
		{
			tau(6-i) = joint_torque_commanded[i]-(tau_accel(i)-tau_gravity(i));
		}

        // Use Moore-Penrose pseudo inverse to calculate external forces
        Eigen::MatrixXd conditioner = Eigen::MatrixXd::Identity(6,6);

        // Use a Marquardt-Levenberg coefficient to dampen the pseudoinverse
		F = (J*J.transpose()+.00001*conditioner).inverse()*J*tau;
		// F = (J.transpose()*J).inverse()*J.transpose()*tau; //I think this is the wrong pseudoinverse

		std::cout<<"Force at the EE:\n";	
		//std::cout<<F(3)<<"\n"<<F(4)<<"\n"<<F(5);
		std::cout<<F;
		std::cout<<"\n\n\n";

		drake::lcmt_robotiq_ft_sensor ft_sensor;
		//ft_sensor.wrench = F;
		
	  	for(int i=0;i<6;i++)
	    {
	      ft_sensor.wrench[i] = F(i);
	    }

	  	my_lcm.publish("FT_SENSOR",&ft_sensor);
	}

	void publish_torques(std::vector<double> desired_position)
	{
	  drake::lcmt_iiwa_command iiwa_command;
	  iiwa_command.num_joints = num_joints;
 	  iiwa_command.num_torques = num_joints;
  	  iiwa_command.joint_torque.resize(num_joints, 0.);
  	  iiwa_command.joint_position.resize(num_joints, 0.);

	  for(int i=0;i<num_joints;i++)
	    {
	      iiwa_command.joint_torque[i] = Kp[i]*(desired_position[i]-joint_position_measured[i]) - Kd[i]*joint_velocity_estimated[i];
   	      iiwa_command.joint_position[i] = 0;
       	  joint_torque_commanded[i] = iiwa_command.joint_torque[i];
       	  
	    }
	  //tau = joint_torque_commanded;
	  my_lcm.publish("IIWA_COMMAND",&iiwa_command);
	  /*
	  std::cout<<"\nPublished Torque command\n";
	  std::cout<<iiwa_command.joint_torque[0]<<" ";
  	  std::cout<<iiwa_command.joint_torque[1]<<" ";
  	  std::cout<<iiwa_command.joint_torque[2]<<" ";
  	  std::cout<<iiwa_command.joint_torque[3]<<" ";
  	  std::cout<<iiwa_command.joint_torque[4]<<" ";
  	  std::cout<<iiwa_command.joint_torque[5]<<" ";
  	  std::cout<<iiwa_command.joint_torque[6]<<"\n";
  	  */
	}
  
    void status_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan,const drake::lcmt_iiwa_status* status)
    {
    	// Estimate the joint velocities
    	dt = (status->utime-utime)/1e6;
    	for(int joint=0;joint<num_joints;joint++)
    	{
    		last_joint_velocity_estimated[joint] = joint_velocity_estimated[joint];
    		joint_velocity_estimated[joint] = (status->joint_position_measured[joint]-joint_position_measured[joint])/dt;
    		joint_acceleration_estimated[joint] = (joint_velocity_estimated[joint]-last_joint_velocity_estimated[joint])/dt;
    	}

    	utime = status->utime;
    	joint_position_measured = status->joint_position_measured;


    	/*
		std::cout<<"\n"<<joint_position_measured[0];
		std::cout<<"  "<<joint_position_measured[1];
		std::cout<<"  "<<joint_position_measured[2];
		std::cout<<"  "<<joint_position_measured[3];
		std::cout<<"  "<<joint_position_measured[4];
		std::cout<<"  "<<joint_position_measured[5];
		std::cout<<"  "<<joint_position_measured[6];

		std::cout<<"\n"<<joint_velocity_estimated[0];
		std::cout<<"  "<<joint_velocity_estimated[1];
	 	std::cout<<"  "<<joint_velocity_estimated[2];
		std::cout<<"  "<<joint_velocity_estimated[3];
		std::cout<<"  "<<joint_velocity_estimated[4];
		std::cout<<"  "<<joint_velocity_estimated[5];
		std::cout<<"  "<<joint_velocity_estimated[6]<<"\n";
		*/
    }
  
    // Callback for IIWA_QDES_COMMAND messages
    void command_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan,const drake::lcmt_iiwa_command* command)
    {
    	// Retrieve the desired joint torques from the IIWA_QDES_COMMAND message
    	std::cout<<"\nReceived qdes command\n";
		qdes.resize(num_joints);
		qdes = command->joint_position;

    }




    void spin()
    {
    	while(1)
    	{
    		my_lcm.handle();
    		publish_torques(qdes);
    		//calculate_ee_force();
    		//get_fk();
    		get_engine_pose();
    		//task_space_transform();
    	}
    }

};



int main()
{

	ImpedanceController my_impedance_controller;
	std::cout<<"\nInitialized\n";
	my_impedance_controller.spin();

	return 0;
}
