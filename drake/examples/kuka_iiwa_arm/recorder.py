from director import consoleapp
from director import lcmUtils
from director import robotstate
from collections import deque
import scipy.io as sio
import time
import atexit

import drake as lcmdrake

class Recorder:
        def __init__(self):
                self.q_hist = deque()
                self.tau_hist = deque()
                print "Initialized Simple Subscriber"
                
		self.iiwa_status_subscriber = lcmUtils.addSubscriber('IIWA_STATUS',lcmdrake.lcmt_iiwa_status,self.onIiwaStatus)

                self.iiwa_command_subscriber = lcmUtils.addSubscriber('IIWA_COMMAND',lcmdrake.lcmt_iiwa_command,self.onIiwaCommand)




	def onIiwaStatus(self,msg):
    	        q = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0] + list(msg.joint_position_measured)
                self.q_hist.append(q)

        def onIiwaCommand(self,msg):
    	        tau = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0] + list(msg.torque_commanded)
                self.tau_hist.append(tau)


   	def write_file(self,filename):
                data = {'q':self.q_hist,'tau':self.tau_hist}
                sio.savemat(filename,data)
                print "Wrote data to ",filename

if __name__=='__main__':
	recorder = Recorder()
        trash = raw_input("press enter to finish")
        print trash
        recorder.write_file('./test_file2')



