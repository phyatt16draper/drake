/// @file
///
/// Implements a simulation of the KUKA iiwa arm.  Like the driver for the
/// physical arm, this simulation communicates over LCM using lcmt_iiwa_status
/// and lcmt_iiwa_command messages. It is intended to be a be a direct
/// replacement for the KUKA iiwa driver and the actual robot hardware.

#include <memory>

#include <gflags/gflags.h>

#include "drake/common/drake_assert.h"
#include "drake/common/drake_path.h"
#include "drake/examples/kuka_iiwa_arm/iiwa_common.h"
#include "drake/examples/kuka_iiwa_arm/iiwa_lcm.h"
#include "drake/examples/kuka_iiwa_arm/sim_diagram_builder.h"
#include "drake/lcm/drake_lcm.h"
#include "drake/lcmt_iiwa_command.hpp"
#include "drake/lcmt_iiwa_status.hpp"
#include "drake/multibody/parsers/urdf_parser.h"
#include "drake/multibody/rigid_body_plant/drake_visualizer.h"
#include "drake/multibody/rigid_body_plant/rigid_body_plant.h"
#include "drake/multibody/rigid_body_tree_construction.h"
#include "drake/systems/analysis/simulator.h"
#include "drake/systems/controllers/inverse_dynamics_controller.h"
#include "drake/systems/framework/diagram.h"
#include "drake/systems/framework/diagram_builder.h"
#include "drake/systems/framework/leaf_system.h"
#include "drake/systems/lcm/lcm_publisher_system.h"
#include "drake/systems/lcm/lcm_subscriber_system.h"
#include "drake/systems/primitives/constant_vector_source.h"
#include "drake/systems/primitives/adder.h"

#include "drake/examples/kuka_iiwa_arm/iiwa_world/world_sim_tree_builder.h"

#include "drake/manipulation/schunk_wsg/schunk_wsg_constants.h"
#include "drake/manipulation/schunk_wsg/schunk_wsg_lcm.h"
#include "drake/lcmt_schunk_wsg_command.hpp"
#include "drake/lcmt_schunk_wsg_status.hpp"

#include "drake/multibody/rigid_body_plant/contact_results_to_lcm.h"
#include "drake/lcmt_contact_results_for_viz.hpp"



DEFINE_double(simulation_sec, std::numeric_limits<double>::infinity(),
              "Number of seconds to simulate.");
DEFINE_string(urdf, "", "Name of urdf to load");

namespace drake {
namespace examples {
namespace kuka_iiwa_arm {
namespace {

using systems::ConstantVectorSource;
using systems::Context;
using systems::Diagram;
using systems::DiagramBuilder;
using systems::DrakeVisualizer;
using systems::RigidBodyPlant;
using systems::Simulator;

using manipulation::schunk_wsg::SchunkWsgStatusSender;
using manipulation::schunk_wsg::SchunkWsgTrajectoryGenerator;

const char* const kIiwaUrdf =
    "/manipulation/models/iiwa_description/urdf/"
    "iiwa14_polytope_collision.urdf";










int DoMain() {
  drake::lcm::DrakeLcm lcm;
  DiagramBuilder<double> builder;


  // This is Phil Code -----------------------------------------------------------------------------

  auto tree_builder = std::make_unique<WorldSimTreeBuilder<double>>();

  tree_builder->StoreModel("iiwa", kIiwaUrdf);

  tree_builder->StoreModel("wsg","/manipulation/models/wsg_50_description/sdf/schunk_wsg_50.sdf");
  
  tree_builder->StoreModel("table",
                           "/examples/kuka_iiwa_arm/models/table/"
                           "extra_heavy_duty_table_surface_only_collision.sdf");
  tree_builder->StoreModel(
      "funnel",
      "/examples/kuka_iiwa_arm/models/funnel/funnel.urdf");
  
  tree_builder->StoreModel(
      "engine",
      "/examples/kuka_iiwa_arm/models/engine/engine.urdf");

  //tree_builder->StoreModel(
  //    "oilbottle",
  //    "/examples/kuka_iiwa_arm/models/objects/oilbottle.urdf");

  tree_builder->AddFixedModelInstance("table",
                                      Eigen::Vector3d(0,.25,0) /* xyz */,
                                      Eigen::Vector3d::Zero() /* rpy */);
  tree_builder->AddFixedModelInstance("table",
                                      Eigen::Vector3d(0.7, .25, 0) /* xyz */,
                                      Eigen::Vector3d::Zero() /* rpy */);
  
  // tree_builder->AddFixedModelInstance("table",
  //                                     Eigen::Vector3d(0, -0.8, .2) /* xyz */,
  //                                     Eigen::Vector3d::Zero() /* rpy */);
  // tree_builder->AddFixedModelInstance("table",
  //                                     Eigen::Vector3d(0.8, 0.8, .2) /* xyz */,
  //                                     Eigen::Vector3d::Zero() /* rpy */);
  // tree_builder->AddFixedModelInstance("table",
  //                                     Eigen::Vector3d(-0.8, -0.8, .2) /* xyz */,
  //                                     Eigen::Vector3d::Zero() /* rpy */);
  // tree_builder->AddFixedModelInstance("table",
  //                                     Eigen::Vector3d(0.8, -0.8, .2) /* xyz */,
  //                                     Eigen::Vector3d::Zero() /* rpy */);
  // tree_builder->AddFixedModelInstance("table",
  //                                     Eigen::Vector3d(-0.8, 0.8, .2) /* xyz */,
  //                                     Eigen::Vector3d::Zero() /* rpy */);
  // tree_builder->AddFixedModelInstance("table",
  //                                     Eigen::Vector3d(0, 0.8, .2) /* xyz */,
  //                                     Eigen::Vector3d::Zero() /* rpy */);
  
  tree_builder->AddFixedModelInstance("engine",
                                     Eigen::Vector3d(0.5,-0.8, .5) /* xyz */,
                                     Eigen::Vector3d(1.5707, 0.0, .5+-1.5707) /* rpy */);
  
  tree_builder->AddGround();

  const double kTableTopZInWorld = 0.736 + 0.057 / 2;
  const Eigen::Vector3d kRobotBase(-0.243716, -0.625087, kTableTopZInWorld);
  //const Eigen::Vector3d kOilbottleBase(1 + -0.43, -0.45, kTableTopZInWorld + 0.3);
  //const Eigen::Vector3d kFunnelBase(1 + -0.43, -0.75, kTableTopZInWorld + 0.1);

  //tree_builder->AddFloatingModelInstance("funnel",kFunnelBase,
  //                       Vector3<double>(0,0,0));
  //tree_builder->AddFloatingModelInstance("oilbottle",kOilbottleBase,
  //                       Vector3<double>(-1.5708,0,0.0));

  int iiwa_id = tree_builder->AddFixedModelInstance("iiwa", kRobotBase);

  //int wsg_id = tree_builder->AddModelInstanceToFrame(
  //             "wsg", Eigen::Vector3d::Zero(), Eigen::Vector3d::Zero(),
  //             tree_builder->tree().findFrame("iiwa_frame_ee"),
  //             drake::multibody::joints::kFixed);

  auto plant = builder.AddSystem<RigidBodyPlant>(std::move(tree_builder->Build()));

  // This ends Phil Code ---------------------------------------------------------------------------














  // Adds a plant.
  
  //RigidBodyPlant<double>* plant = nullptr;
  const std::string kModelPath = "/manipulation/models/iiwa_description/urdf/"
      "iiwa14_polytope_collision.urdf";
  const std::string urdf = (!FLAGS_urdf.empty() ? FLAGS_urdf :
                            GetDrakePath() + kModelPath);

  const RigidBodyTree<double>& tree = plant->get_rigid_body_tree();
  //const int num_joints = tree.get_num_positions();
  const int num_joints = 7;

  // Turn off all the gains, so the acceleration this block spits out is the
  // grav comp torque.
  auto pure_grav_comp =
      builder.AddSystem<systems::InverseDynamicsController<double>>(
          urdf, nullptr, VectorX<double>::Zero(7), VectorX<double>::Zero(7), VectorX<double>::Zero(7),
          false /* without feedforward acceleration */);
  auto zero_state_source = builder.AddSystem<ConstantVectorSource>(
      VectorX<double>::Zero(14));

  builder.Connect(plant->model_instance_state_output_port(iiwa_id),
                  pure_grav_comp->get_input_port_estimated_state());
  builder.Connect(zero_state_source->get_output_port(),
                  pure_grav_comp->get_input_port_desired_state());


  // Your command comes in here.
  auto command_sub = builder.AddSystem(
      systems::lcm::LcmSubscriberSystem::Make<lcmt_iiwa_command>(
          "IIWA_COMMAND", &lcm));
  command_sub->set_name("command_subscriber");

  // This is the joint torque command on top of the gravity torque.
  // Check iiwa_lcm.h IiwaTrqCommandReceiver
  auto command_receiver =
      builder.AddSystem<IiwaTrqCommandReceiver>(num_joints);
  command_receiver->set_name("command_receiver");
  builder.Connect(command_sub->get_output_port(0),
                  command_receiver->get_input_port(0));


  // Add the grav comp + your command.
  auto adder = builder.AddSystem<systems::Adder<double>>(2, 7); // 2 inputs, each with 7 dof.
  builder.Connect(command_receiver->get_output_port(0), adder->get_input_port(0));
  builder.Connect(pure_grav_comp->get_output_port_control(), adder->get_input_port(1));
  builder.Connect(adder->get_output_port(), plant->get_input_port(0));

  // Add visualizer.
  auto visualizer = builder.AddSystem<systems::DrakeVisualizer>(tree, &lcm);
  visualizer->set_name("visualizer");
  visualizer->set_publish_period(kIiwaLcmStatusPeriod/5.0);
  builder.Connect(plant->get_output_port(0),
                  visualizer->get_input_port(0));

  // Publish status.
  auto status_pub = builder.AddSystem(
      systems::lcm::LcmPublisherSystem::Make<lcmt_iiwa_status>(
          "IIWA_STATUS", &lcm));
  status_pub->set_name("status_publisher");
  status_pub->set_publish_period(kIiwaLcmStatusPeriod/5.0);
  auto status_sender = builder.AddSystem<IiwaStatusSender>(num_joints);
  status_sender->set_name("status_sender");

  builder.Connect(plant->model_instance_state_output_port(iiwa_id),
                  status_sender->get_state_input_port());
  builder.Connect(zero_state_source->get_output_port(),
                  status_sender->get_command_input_port());
  builder.Connect(status_sender->get_output_port(0),
                  status_pub->get_input_port(0));

  // Contact visualization
  auto contact_viz =
      builder.AddSystem<systems::ContactResultsToLcmSystem<double>>(
          plant->get_rigid_body_tree());
  auto contact_results_publisher = builder.AddSystem(
      systems::lcm::LcmPublisherSystem::Make<lcmt_contact_results_for_viz>(
          "CONTACT_RESULTS", &lcm));
  contact_results_publisher->set_publish_period(kIiwaLcmStatusPeriod/5.0);
  // Contact results to lcm msg.
  builder.Connect(plant->contact_results_output_port(),
                  contact_viz->get_input_port(0));
  builder.Connect(contact_viz->get_output_port(0),
  contact_results_publisher->get_input_port(0));

  auto sys = builder.Build();

  Simulator<double> simulator(*sys);

  lcm.StartReceiveThread();
  simulator.set_publish_every_time_step(false);
  simulator.Initialize();

  // Simulate for a very long time.
  simulator.StepTo(100000);

  return 0;
}

}  // namespace
}  // namespace kuka_iiwa_arm
}  // namespace examples
}  // namespace drake

int main(int argc, char* argv[]) {
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  return drake::examples::kuka_iiwa_arm::DoMain();
}
