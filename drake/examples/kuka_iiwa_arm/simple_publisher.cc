/*
This is just a simple Impedance Controller to drive the KUKA iiwa
arm to joint positions published on IIWA_QDES_COMMAND
*/

#include <iostream>
#include <lcm/lcm-cpp.hpp>
#include "drake/lcmt_iiwa_command.hpp"
#include "drake/lcmt_iiwa_status.hpp"


class ImpedanceController {
	lcm::LCM my_lcm;
	drake::lcmt_iiwa_status pub_msg;

public:
	// The timestamp in microseconds.
	int utime;
 	std::vector<double> joint_position_measured;
  	std::vector<double> joint_position_commanded;
   	std::vector<double> joint_velocity_estimated;
	std::vector<double> qdes;
   	double dt;
	static const int num_joints = 7;
	double Kp[num_joints] = {2,2,2,2,1,1,1};
   	double Kd[num_joints] = {1.9,1.9,1.9,1.9,.1,.1,.1};

	void initialize()
	{
		my_lcm.subscribe("IIWA_STATUS",&ImpedanceController::status_cb,this);
		my_lcm.subscribe("IIWA_QDES_COMMAND",&ImpedanceController::command_cb,this);
		joint_position_measured.resize(num_joints,0);
		joint_position_commanded.resize(num_joints,0);
		joint_velocity_estimated.resize(num_joints,0);
		qdes.resize(7);
		for(int i=0;i<num_joints;i++)
		{
			qdes[i] = 0;
		}

	}

	void publish_torques(std::vector<double> desired_position)
	{
	  drake::lcmt_iiwa_command iiwa_command;
	  iiwa_command.num_joints = num_joints;
 	  iiwa_command.num_torques = num_joints;
  	  iiwa_command.joint_torque.resize(num_joints, 0.);
  	  iiwa_command.joint_position.resize(num_joints, 0.);

	  for(int i=0;i<num_joints;i++)
	    {
	      iiwa_command.joint_torque[i] = Kp[i]*(desired_position[i]-joint_position_measured[i]) - Kd[i]*joint_velocity_estimated[i];
   	      iiwa_command.joint_position[i] = 0;
	    }
	  my_lcm.publish("IIWA_COMMAND",&iiwa_command);
	  /*
	  std::cout<<"\nPublished Torque command\n";
	  std::cout<<iiwa_command.joint_torque[0]<<" ";
  	  std::cout<<iiwa_command.joint_torque[1]<<" ";
  	  std::cout<<iiwa_command.joint_torque[2]<<" ";
  	  std::cout<<iiwa_command.joint_torque[3]<<" ";
  	  std::cout<<iiwa_command.joint_torque[4]<<" ";
  	  std::cout<<iiwa_command.joint_torque[5]<<" ";
  	  std::cout<<iiwa_command.joint_torque[6]<<"\n";
  	  */
	}
  
    void status_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan,const drake::lcmt_iiwa_status* status)
    {
    	// Estimate the joint velocities
    	dt = (status->utime-utime)/1e6;
    	//if(dt>2){dt=.005;}
    	for(int joint=0;joint<num_joints;joint++)
    	{
    		joint_velocity_estimated[joint] = (status->joint_position_measured[joint]-joint_position_measured[joint])/dt;
    	}

    	utime = status->utime;
    	joint_position_measured = status->joint_position_measured;
    	joint_position_commanded = status->joint_position_commanded;

    	
		std::cout<<"\n"<<joint_position_measured[0];
		std::cout<<"  "<<joint_position_measured[1];
		std::cout<<"  "<<joint_position_measured[2];
		std::cout<<"  "<<joint_position_measured[3];
		std::cout<<"  "<<joint_position_measured[4];
		std::cout<<"  "<<joint_position_measured[5];
		std::cout<<"  "<<joint_position_measured[6];

		std::cout<<"\n"<<joint_velocity_estimated[0];
		std::cout<<"  "<<joint_velocity_estimated[1];
	 	std::cout<<"  "<<joint_velocity_estimated[2];
		std::cout<<"  "<<joint_velocity_estimated[3];
		std::cout<<"  "<<joint_velocity_estimated[4];
		std::cout<<"  "<<joint_velocity_estimated[5];
		std::cout<<"  "<<joint_velocity_estimated[6]<<"\n";
		
    }
  
    // Callback for IIWA_QDES_COMMAND messages
    void command_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan,const drake::lcmt_iiwa_command* command)
    {
    	// Retrieve the desired joint torques from the IIWA_QDES_COMMAND message
    	std::cout<<"\nReceived qdes command\n";
		qdes.resize(num_joints);
		qdes = command->joint_position;

	// Calculate impedance controller torque and publish that torque
	publish_torques(qdes);
    }

    void spin()
    {
    	while(1)
    	{
    		my_lcm.handle();
    		publish_torques(qdes);
    	}
    }

};

int main()
{

	ImpedanceController my_impedance_controller;
	my_impedance_controller.initialize();
	std::cout<<"\nInitialized\n";
	my_impedance_controller.spin();

	return 0;
}
